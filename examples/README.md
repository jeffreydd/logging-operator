## ClusterOutput with Elasticsearch

This example will result in an example app logging to the logging operator's pre-defined cluster output, which is Elastic in this case.

An example logging generator demo app is provided by Banzai which can be deployed using provided [deployment](log-generator-deployment.yml). It can be used in combination with the [example flow](demo-flow.yml) provided in this repository.

```
kubectl create ns lpctest-logging-demo
kubectl apply -n lpctest-logging-demo -f log-generator-deployment.yml
kubectl apply -n lpctest-logging-demo -f demo-flow.yml
```
